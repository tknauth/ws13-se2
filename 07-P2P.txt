Peer-to-Peer Networks (structured vs unstructured P2P systems)

Most research and activity around p2p systems was in early 2000s.

P2P dillutes the client/server distinction: clients offer services to
each other, either replacing or adding to server-side functionality.
Example: Napster (centralized); Gnutella, KaZaA (decentralized);
Pastry, Chord, CAN (structured p2p); Skype, BitTorrent

Napster: sharing content, e.g., music, between users. Users store
content, central server has index, i.e., mapping of files to users.
Quick searching for files on central server. Users come and go
(churn). Makes keeping centralized database up-to-date a challenge.

Gnutella had the same intentions as Napster, i.e., allow users to
share files with each other. However, completely decentralized
architecture. Each application instance participates in storing,
searching, and serving files as well as routing queries to neighboring
nodes.

Searching for files by flooding, i.e., sending request to neighboring
node. Out-degree is 7, i.e., if node does have a file, forwards
request to 7 of its neighbors. Maximum of 10 forwards before request
is dropped (unsuccessful search). Flooding, i.e., uninformed
forwarding, poses scalability problems. Has loop detection, i.e., if
seen query already, drop it immediately without forwarding again.
Responses are sent along the query forwarding path.

Has to deal with firewalls. Easy if one of the participants is not
behind a firewall: the firewalled node initiates the connection.
Establishing a connection typically allowed behind a firewall. Push vs
pull mode depending on whether querying node, i.e., node that wants to
download a file, is the firewalled node.

If sending and receiving node are firewalled, one solution is to use a
non-firewalled push proxy. Sender and receiver can both talk to the
push proxy. Proxy is relaying communication between firewalled nodes.

How to connect to the Gnutella network? Bootstrapping, i.e,
establishing a connection to the network, sends ping message to
existing network nodes. Node's IP addresses are cached from previous
runs of the client. Alternatively, use DNS to learn IP addresses of
some participating nodes. Node replies with pong message, if it has
spare capacity. It is OK to leave pong messages unanswered.

Use round-trip time of ping-pong message to establish "neighborhood",
i.e., which nodes are close and responsive vs which nodes are remote
and slow to answer.

KaZaA as alternative to Gnutella. Same intent: search and download
media files. Clients can download in parallel from multiple sources.
Transparent switching of servers, if it becomes unavailable. User can
set limit on maximum parallel up/down connections. Queue management on
server and clients: frequent uploaders get priority at server queue.

KaZaA used a proprietary communication protocol with encrypted payload
and control data. Messages embedded in HTML, similar to REST. Network
topology is hierarchical. Each node is either super-node or descendent
which are assigned to super-nodes.

KaZaA binary has built-in list of known super-nodes. On connect, ask
super-node for updated super-node list. Try connect to some, e.g.,
five, of the known super-nodes. Use the super-node with fastest round
trip time (RTT).

Queries are first sent to super-node which responds with matches. If
reply has enough matches, clients can set minimum of desired answers,
super-node forwards query to other super-nodes.

Distributed hash tables (DHTs). Problem with p2p systems is that
unpopular content may be hard to find, even though some node in the
network is storing it. Research around DHTs is always concerned with
how quickly, measured in number of hops, we can find a certain item.

Simple search strategy: expanding ring search. If r of N nodes have
copy, search cost is at least N/r (on average), i.e., O(N).
Alternative, is to have a central index, e.g., Napster. This, however,
may be hard to scale. But, we can do better than O(N).

Directed searches. Item determines which network node is responsible
for storing it. Use item to lookup nodes that are (supposed to) store
it. System must (a) evenly distributed items across nodes, and (b) be
able to cope with churn, i.e., clients constantly joining and leaving
the network.

Use item name to compute hash. Use hash as index in lookup table
(standard hash table). Expect #items/#buckets items per hash table
index. Does not work for DHTs because of clients constantly joining
and leaving network. Would have to re-assign keys/items to nodes
constantly.

Solution is to use consistent hashing. Number of buckets is
independent of # nodes in network. Each hash value is associated with
a network node by "closeness"/proximity of the node's ID to the hash
value.

Must know all nodes to insert/lookup data in DHT. Works well with
small and static server population. In KaZaA, for example, search
requests must be forwarded because nodes only have a local view of the
topology. Answer to query may sit on a server that is not in the
immediate neighborhood of the searching node.

Design critera for a good DHT. Number of hops to find an item should
be small. Small localized neighborhood. Routing messages across
overlay should be decentralized. Graceful handling of nodes joining
and leaving network. Must provide low stretch: tradeoff between
routing hops and unicast latency. Provides simple interface:
lookup(key) -> data. Very generic. Keys have no semantic meaning, data
can be anything.

Chord (MIT). Circular m-bit ID space for nodes and keys. Node is SHA-1
of IP address. Key ID also SHA-1. Key assigned to node with same ID or
follows the key ID. Each node responsible for keys/nodes. Key/nodes
items move on join or leave.

How key lookup works in Chord: each node knows predecessor and
successor node, i.e., node with next lower and higher ID than itself.
Lookup requests are forwarded using successor nodes. Needs N/2 hops on
average, i.e., complexity is O(N).

Speed up lookup by maintaining a finger table at each node. Finger
extends knowledge of nodes beyond simple successor/predecessor
concept. Finger table has m entries, where m is number of bits in key
space ID. Each entry point to node which is closest to ID: self+2^0,
self+2^1, self+2^2, ..., self+2^(m-1). Cuts complexity to O(log N)
hops per lookup.

--- end of lecture --- continued on 06-01-2014 ---

log(n) hops required to lookup a key in Chord thank to the finger/routing table.

Must maintain the finger table in the face of joining and leaving
nodes. For correctness, successor and predecessor of a node must be up
to date. Otherwise, key lookup might fail. Finger table only required
for fast lookups (but not for correctness).

Each node joining the ring must do three things: (1) initialize all
fingers of the new node, (2) update finger table of existing nodes,
and (3) transfer key responsibility.

Initialize finger table by using any known ring node to do the lookups
for j+2^0, j+2^1, j+2^2, ... Lookup results are used to populate node
j's finger table.

Nodes in the range [pred(j)-2^i+1, j-2^i] must update their finger
table. Must update log(N) nodes.

Transfer key responsibility. Cannot delete key right after transfer,
but must wait until forwarding table of nodes pointing to the
transfered key are updated.

Stabilization: (1) finger tables are reasonably fresh, (2) successor
pointers are correct (don't care about finger tables), (3) successor
pointers are inaccurate or key migration is incomplete. Stabilization
algoritham periodically verified and refreshes node pointers
(including fingers). Eventually stabilizes the system when no node
joins or fails.

Dealing with failures. Node failures may prevent correct lookup of a
key. Each node maintains a successor list, of length r, of immediate
successors. Successor list guarantees correct lookup with certain
probabilty. Can compute length of successor list for some target
probability.

P(no broken node) = (1-0.5^r)^N - plug-in probability and solve for r
to determine length of successor list.

Stability. How long does it take to reach 2*N nodes? N/2 nodes?
Half-life is minimum of reaching 2*N or N/2. Theorem: any N-node P2P
network with high connection probability must notify every node with
an average of \Omega(log N) nodes per \tau

Chord and network topology: logically close nodes, i.e., nodes with
adjecent IDs, may be very far apart geographically. There is no
correlation between logical and physical node distance.

Chord summary: can only search for exact key. Cannot search for all
keys greater than X. Needs log N steps to locate a key (with high
probability, if finger tables are up-to-date). Replication may keep
additional copies in neighboring nodes.

Pastry is another P2P network developed by Microsoft Research.
Circular m-bit ID space for keys and nodes. Addresses in base 2^b with
m/b digits. Variable address length allows to trade-off routing table
size and lookup overhead. Node ID is SHA1 of IP address. Key ID also
SHA1 of key. Key is mapped to node whose ID is numerically closest to
the key ID. This allows joining nodes to accept transfered keys from
both it successor and predecessor.

Pastry lookup. Prefix routing from A to B. At h-th hop, arrive at node
that shares prefix with B of length of at least h digits. Example:
A=5324 routes to B=0629 via 5324 -> 0748 -> 0605 -> 0620 -> 0629. If
there is no such node, forward message to neighbor numerically closer
to destination (successor). Requires log_{2^b}(n)

Pastry state and lookup. Node N0201 needs entries for N1???, N2???,
N3???, N00??, N01??, N03??, N021?, N022?, N023?,  N0200, N0202, N0203.

Lookup algorithm. If key D is in range of the leaf set. Either forward
to node in routing table where shared prefix is longer than the
current matching prefix. If there is no entry with longer prefix,
forward to node with numerically closer ID.

Lookup performance. ceil(log_{2^b} N) steps if message forwarded using
routing table. If key in leaf set, routing finished with a single hop.

Joining nodes and how rounting tables are initialized. Node X joins
the ring. X contacts node A (close to X) and sends A a "join" message.
"join" message is forwarded to numerically closest node. Last node
sends an answer to X, containing the routing table.

Getting the routing table from the numerically closest node is good to
maintain the locality. Routing table of joining node will be
initialized with nodes close to it.

Q: why small geographic steps in beginning and large steps at the
later stages?

Another p2p system is CAN (from Berkeley). Cartesian d-dimensional
space (d-torus). Incrementally split space between joining nodes.
Mapping from key to node by hashing key for each dimension.

Lookups need (dN^(1/d))*4.

--- end of lecture --- continued on 13-01-2014 ---

Lookup complexity is based on root of N. For d dimensions there are on
average N^(1/d) nodes in each dimension. Can speed up lookup by
introducing more dimensions. More dimensions also means more state to
keep.

CAN introduced concept of landmark routing. Nodes do not have a
pre-defined ID, i.e., IDs can be assigned. Nodes are placed according
to locality. Can use round trip time measurements with well-known
servers, called landmarks, to determine an ordering. With m landmark
server, there are m! (m factorial) possible orderings. Nodes with same
or similar ordering are placed close together in the CAN node space.

CAN uses locality of nodes to build the overlay, while Chord,
randomly, determines a node's function based on its IP address.

DHTs are a simple yet powerful abstraction. Can build many useful
systems on top of it, e.g., key/value stores, file systems,
application-layer multicast, and distributed caches. Key properties
and trade-off points revolve around lookup speed (# of hops), state
size (degree), and ease of management. Successful DHT must cope with
rapid membership changes, i.e., nodes constantly joining/leaving the
network.

Content distribution with BitTorrent (also useful for legal purposes,
not just to share movies!)

Many users downloading large images from central server. Puts enormous
load on the servers. Hard to scale robustly the server farms. Content
popularity often Zipf distributed, i.e., few very popular files, lots
and lots of marginally popular content.

Users are well connected. Idea is to blur the notion of
client/server. Each client becomes a server, in a sense, by allowing
other clients to also download from them.

More real world legal uses which could benefit from BitTorrent-like
approach: anti-virus definition, distribution of streaming content,
Linux distribution images.

Can piggy-back on the client's resources, e.g., their bandwidth and
storage. Also makes the system more fault-tolerant. However, client
population will be heterogeneous and changing. Co-operative content
distribution may dramatically reduce the time until all clients have
received the content, e.g., from many hours down to a few minutes.

Based on swarming concept: server sends different part of file to each
client. Clients exchange the parts between themselves.

Browser downloads .torrent file, containing initial information about
the content, e.g., which trackers are responsible for the
file. Tracker knows at least one peer with complete file. Once a peer
downloaded entire file, it becomes a seeder. Downloading clients are
called leecher. Trackers record downloading progress of each
peer. Peers periodically contact tracker for updated peer list.

On joining a download, tracker randomly selects some peers to send to
the new client. Once a peer is incorporated successfully in a session,
it may also become part of an initial peer set. Peer sets evolve over
time. Large temporal diversity in peer set, i.e., good mix of old and
new peers. Ensures a "generational" transfer of data.

Each peer keeps two variables about the state of each connection:
interested and chocked, don't want to send/recv any data from you
anymore.

Chunk selection algorithm determines which chunks to download
next. Could select chunks randomly. Could also have an informed
strategy, e.g., download rarest chunks first. Rare chunks can more
easily traded with other peers. Maximize the minimum number of copies
of a given chunk in each peer set. BT uses random selection for new
peers, later switching to a rarest-first strategy.

Peer selection strategy seeks to minimize the number of peers actively
communicated with (reduces overhead). Use "tit for tat" with leechers,
and best d/l rate for seeders. Randomly "unchoke" a peer to give other
peers the opportunity to give better service to me. BT (un)chokes
periodically every 10s (long enough for TCP to establish a
steady-state).

BT study. 5 months tracker log of popular BT session. Logged activity
on the tracker and a few instrumented clients. Data volume uploaded by
seeders is 2x, 40 TB, compared to data uploaded by leechers, only 20
TB, over monitoring period. Seeder population was always >= 20\% of
peer population.

--- end of lecture --- continued on 2014-01-20 ---
