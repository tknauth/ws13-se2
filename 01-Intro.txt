focus on large scale, scalable system; use case: more users, more
requests/seconds, more data (web index); more data produces than ever;
not just computer science, e.g., bio/life sciences; SE1 is about the
principles to build infrastructures to deal with vast amounts of data;
challange is to build reliable systems out of unreliable, commodity
components; how big is "big data"? e.g., facebook deals with petabytes

redundancy in storage; one TB of data per disk (incl. 3x redundancy);
petabyte ~= 1k disks

how do we make predictions? moore's law: # transistors doubles every
18 month; similar predictions for storage

digital universe gap: in the future, more data is generated than can
be stored; exact number may differ, but there will be more data for
sure

peter norvig: don't need models anymore. data substitutes model

availability model: N machines, probability machine is up p, N
machines up p^N, probability all machines down (1-p)^N; basic
assumption of independant failures; however, rarely the case, e.g.,
all machines share same power line

must deal with slow and failed machines;

example of an extremely scalable system: Google search

processing a querry on a single machine not an option: would take tens
of seconds; nobody likes to wait; hence, must parallelize, not just on
multiple cores, but multiple machines;

what happens during query execution: google.com is resolved via DNS;
DNS gives you a set of IP addresses; in addition google.com may be
resolved to an alias first; why important? first round of load
balancing done via DNS, e.g., round robin, i.e., set of IP addresses
changes over time; requests from different users may end up in
different data centers; large number of requests gets split up for the
first time

can combine DNS round robin with geo-location; dispatch request to DC
close to the user

If you control the DNS server, can redirect user on many metrics,
e.g., load of DC, user's location

DNS returns IP address of a hardware load balancer. HW load balancer
forwards request to Google web server (GWS, something that speaks
HTTP, e.g., maybe Apache).

Can also do load balancing in software, e.g., use Apache with reverse
proxy module.

GWS queries index server (IS). IS has (part of) inverted index (word,
list(url, score)). IS computes hit list using search terms and
inverted index. IS computes hit list for each term. Intersect results
and compute their score. IS sends list to GWS.

CPU speed not the only limiting factor. Would like to keep index in
memory. A single machine does not have enough memory. 10k machines may
have.

Partition/shard the data. Each machine holds a subset/shard/partition
of the entire index. Computations can proceed independently on each
subset/shard/partition.

Single shard may fit in memory of a single machine, but machine may
not have enough compute power to handle all requests for this
shard. Replicate (identical copies) the shard across multiple
machines. Distribute requests for each shard across all machines for
this shard. Number of shards determins how much processing each
machine has to do. More server, smaller shards, less processing.

GWS broadcasts querry to all shards (remember, there are multiple
machines per shard!). Each set of shard servers has an additional load
balancer (LB). This LB distributes the queries among the servers of a
single shard. Shard server sends results directly to GWS.

More requests, increase number of machines per shard. If size of index
increases, add more shards, to keep per-shard data constant. Reaching
DC limit, add DC and add to list of IPs returned by DNS.

Now we know the relevant pages for our search terms. Must still
render/prepare the results page. Another set of servers holds the
actual documents for a URL. GWS talks to document server after
receiving URL list from IS. Document servers use same strategy as
IS. Randomly distribute documents across servers (sharding), multiple
servers per shard, load balance between shard servers.

Fault tolerance (FT): easier because index data is read-only (from the
view of the GWS, IS, and document servers). But, somebody has to
update the index. Interesting interactions between updates and
replication for FT. Copies must stay synchronized.

Re-sharding is also a problem. URLs will move between shards. How to
keep information up to date about which shard is responsible for which
URL (one possible solution: consistent hashing).

Updating IS must happen atomically. Control LB, take out IS, LB will
not send any requests to IS. IS updates. When done, IS sends LB
another message to indicate that update is finished.
