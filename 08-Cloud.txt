Cloud Computing

Rent resources, CPU, memory, and disk, on-demand from a resource
provider. As opposed to buying them. Provider can leverage economies
of scale, i.e., reduce cost of operting a massive IT infrastructure.
Customers, e.g., businesses, can save money because they no longer
need an in-house IT department.

Customer only pays for the actually used/consumed resources. Provider
must meter the consumption of resources for each customer. Internet
used to access and deliver rented resources.

Providers make it very easy to get started. Create an account and
enter your credit card information. You can start using the provider's
resources right away.

ANSI defined five key characteristics for cloud computing: (1)
self-service, (2) ubiquitous network access, (3) metered use, (4)
elasticity, (5) resource pooling. 

Elasticity: cloud gives you the illusion of infinite resources. Need
more CPU and RAM? Just spin up a new VM. No capital needlessly tied up
in hardware investments, i.e., servers rotting away in your company's
basement.

Resource pooling: cloud provider can leverage economies of scale. May
be able/willing to hand down provider-side savings, achieved by
efficient resource sharing among multiple customers, to the customers.

Where did it come from? What was before "the Cloud"? One example, grid
computing. Focus mostly on compute-intensive, high performance
computing, tasks. Used to rent resources on an IBM mainframe computer.
With cloud computing, you rent resources on a standard x86 server.

A taxonomy for cloud computing along two dimensions: (1) security
(private, hybrid, public cloud) and (2) kind of resource (IaaS, PaaS,
SaaS).

Resources delivered to customer in the form of virtual machines.
Provider can fit/co-locate multiple VMs on a single server. Overall
goal is to increase average resource utilization of the server. In the
ideal case, customers resource usage complement each other.

Each physical server runs a virtual machine monitor (VMM), e.g., kvm,
Xen, VMware, or HyperV. Some problems related to virtualization:
overhead, and interference. Virtulization does not come for free. In
the past, especially I/O-heavy workloads suffered the most. Also,
resource isolation is not perfect, especially with contending disk
access.

Providers employ over-provisioning to increase average server
utilization. For example, allocate 8 single-core VMs on a 4-core
server. Increases the cloud provider's margin/profit. Potential
downside when all VMs request their resources simultaneously.

Overload situations can be resolved using live migration features of
modern hypervisors. Can move a virtual machine between different
physical servers. Iteratively copy VM's memory pages from source to
destination. Some pages must be copied more than once, if they changed
since the last copy. Remember, while the memory is copied in the
background, the VM keeps running, e.g., serving HTTP requests.

Live migration, as opposed to offline migration, tries to minimize the
time a service within the VM is not available. The unavailability
depends a lot on the memory update rate of the virtual machine. If
this update rate is low, unavailability is short. At the other end of
the spectrum, outages can last up to tens of seconds.

Cloud providers also offer storage, in addition to compute. Object
stores have proliferated with cloud computing. Interface is different
from a traditional file system or database. Can put, get, update
objects via HTTP interface. One example is Amazon's S3.

<pricing>

<api>
Objects are assigned to a "bucket". Bucket is nothing more than a
collection of objects; think of folders in a traditional file system.

Object stores naturally also provide a way to specify who can access
the data using traditional access control lists (ACL). Customer can
specify who can read and write objects.

One other feature is implicit versioning of objects. Can recover old
versions of an object, e.g., after accidental overwrite.

Can combine object store and compute service to distribute a VM image
using BitTorrent. Instead of hosting the image on S3, we can host the
.torrent file on S3 and run a tracker server in the cloud. This will
be much cheaper than paying for the bandwidth client's use to get the
original VM image.

{I, P, S}aaS. Infrastructure, Platform, or Software as a Service.
Infrastructure we already know about: you rent resources in the form
of virtual machines. Platforma as a Service the provider allows you to
host your application on its infrastructure. Application must be
written using a framework provided by the provider, e.g., Google App
Engine or Amazon's Beanstalk.

Software as a Service, e.g., GMail. Users don't install an application
on their desktop/laptop, e.g., traditional e-mail client such as
Thunderbird, but access their applications using a standard web
browser, e.g., GMail (JavaScript, HTML, and CSS).

This delivery model has many enticing properties. Software can be
continuously updated. Whenver you access www.gmail.com, you get the
latest code. Software delivered in this way available for many
different domains, e.g., customer relationship mgmt (salesforce), enterprise
resource planning (???).

One drawback related to cloud computing is data escrow. If your cloud
provider, who has all you business related data, goes bankcrupt, how
do you get your data back? More general, can I move my data/virtual
infrastructure between providers, some new provider may be cheaper.

You have to trust your cloud provider to some degree. You cannot
control when and where your data may be accessed. Also, data centers
may go offline. Must include fault tolerance into applications
deployed in the cloud.

Why now? Ubiquitous broadband network access (also on mobile).
Applications constructed accoring to REST principles. Easily accessed
over HTTP(S). HTTP(S) is a well understood and supported protocol.

Can divide clouds depending on who is allowed to access them: public
cloud anybody can use. Simply create an account. Private cloud are
restricted to, e.g., employees of the company running the private
cloud. Can also have hybrid approaches, e.g, if the companie's private
cloud does not provide sufficient capacity for your workload, also use
resources from the public cloud.

OpenStack is a collection of services to provide platform for anyone
to setup an infrastructure (IaaS) cloud.
