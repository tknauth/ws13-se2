Dremel: Interactive Analysis of Web-Scale Data Sets

Big data analytic (Google scale, peta-bytes); MapReduce has high turn around times, i.e., minutes between job submission and getting an answer. Need for something more "real-time" with support for ad-hoc queries.

Classification of queries in two classes: operational (few rows per query, high number of queries per second) vs analytical (low query frequency, billions of rows per query).

Interactive queries by data analysts. Low response time, e.g., less than a minute (humans are impatient), for queries potentially touching peta-bytes of data.

Have to parallelize query processing. Processing may be limited by hard disk bandwidth. Simple graph where x-axis is # of disks and y-axis cumulative (read) throughput. Need N disks to achieve aggregated Y throughput. Gives lower bound/estimate for number of disks to process X peta-byte in certain amount of time.

Holding data in memory also feasible for peta-byte sized data sets. Can buy machines with 512 GiB RAM.

Storage is cheap. Store everything for later analytics, e.g., server/system logs, user behavior (click stream), sales, ... May enable new insights for company.

Design goals of Dremel: process terabytes of data in a few seconds. Data stored on 1000s of disks, 1000s of CPUs available for processing.

Data stored as nested structures, e.g., tuples inside a list, tree. Must de-/serialize when writing to/reading from disk. In-memory data is not simply [start addr, end addr] as is typical for fixed-sized arrays.

Which serialization format to use? XML is too bloated (size) and takes too long for processing. Need something more efficient. Google rolled their own serialization format called Google Protocol Buffers. Used across many Google products to exchange data. Language agnostic, i.e., binding for many popular languages exist. Binary format, i.e., not directly human readable.

Another alternative, popular in the web-world, to XML and Google Protobufs is JSON. JSON is a middle ground between Protobufs and XML w.r.t. verbosity, speed, and size. One very nice property of Protobufs is direct object creation from serialized state (zero-copy), i.e., no need to parse and construct language level object.

Protobufs require a schema outlining the fields (contents) of a message. Fields can be (i) required (1), (ii) optional (0:1), or (iii) repeated (0:n). The schema is automatically turned into code for your target language, e.g., C or Python, by a compiler. The compiler takes a schema definition and outputs, e.g., C or Python, code. Easy to exchange messages between components written in different languages.

Protobufs also allow evolution of messages, i.e., adding/removing fields. Protobuf library (generated code) take care of potential incompatibilities. May have a component using v2 of message schema talking to 2nd components still using v1.

De-/serialization using Protobuf can be up to 35x faster than JSON according to some benchmarks (taken off github.com).

Example of how to represent web graph using Protocol Buffer data structure. Each document is represented by a unique ID. Relationships between documents are expressed as Forward/Backward entries, each containing the UUID of the successor/predecessor.

Practical feature of Protobufs: can forward, receive, parse older/newer versions of a message (schema evolved). 

Dremel interfaces with Protocol Buffers stored on disk or Bigtable (which also stores data on disk, but in a different format). Queries are written in SQL-like language to make adoption easier for data analyst. Dremel executes part of the query on the storage node, e.g., filter or aggregation. Reduces data travelling across the network.

Dremel uses column data layout. Column oriented data layout also benefits existing data processing frameworks, e.g., MapReduce.

How to do with stragglers? Some processes will always be significantly slower than other, especially if you have a high fanout, e.g., 1000s of parallel executions. With Dremel you can either provide a timeout, i.e., return answer, even if not finished processing, after X seconds. Alternatively, return result when finished processing 99% of data.

Why is Dremel faster than MapReduce? They are data both data processing frameworks after all. Columnar representation helps. Dremel does a better job of caching input data. MapReduce needs multiple runs/iterations for certain SQL-queries, Dremel can compute those queries in a single run. M/R always sucks if you have to iterate on your data, i.e., fix point calculations.

Open source alternatives to proprietary Dremel? Hive and Impala.

Impala runs a daemon on every (data) node. Clients send query to any node, where the query planner executes the individual steps on the individual data nodes. Impala reads data from HDFS. No fault tolerance, as in MapReduce, because intermediate results are not written to disk.

Take aways: interactive queries over terabyte data size is possible; processing frameworks benefit from columnar data store; record assembly and parsing is expensive if done millions of times for each query
